kaggle competitions download -c home-credit-default-risk -p data/raw
cd data/raw
find . -depth -name '*.zip' -exec unzip -n {} \; -delete
cd ../..