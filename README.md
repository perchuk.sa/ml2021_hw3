# ML2021_HW3

ml mipt 2021, homework 3

### Prepare environment

1. Set up virtual environment
```
python3 -m venv env 
source env/bin/activate
```
To deactivate:
```
deactivate
```

2. Install requirements
```
pip install --upgrade pip
pip install -r requirements.txt
```

3. Download data
```
bash data/download_data.sh
```

4. Notebook is located in notebooks/ folder.

*Note: kaggle submissions are commented out!*

### Task

Задание выполняется на датасете Home credit на kaggle https://www.kaggle.com/c/home-credit-default-risk

Вы можете использовать любую реализацию градиентного бустинга по вашему выбору: Lightgbm, Catboost ...
0) Взять три гиперпараметра для градиентного бустинга, и для каждого проварьировать один из них, фиксируя остальные. Построить графики зависимости метрики на кросс-валидации от параметра. Прокомментируйте, какие параметры и как влияют на переобучение.
1) Сделать с помощью GridSearchCV и RandomizedSearchCV отбор наилучших параметров.
2) Построить график feature importance. Какие признаки оказались самыми важными?

**Теперь переходим к стекингу.**

Используем реализацию https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.StackingClassifier.html
1) Построить стекинг для 5 различных моделей по вашему выбору с разными параметрами. Можете взять любые, например 2 бустинга, 2 случайных леса, лог рег. Записать скор на тестовой выборке.
2) Теперь уберем из ансамбля наилучшую модель и снова постройте стекинг. Как изменился результат? Прокомментируйте.